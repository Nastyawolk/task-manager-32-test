package ru.t1.volkova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.data.DataBackupLoadRequest;
import ru.t1.volkova.tm.dto.request.data.DataBackupSaveRequest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    public static final String FILE_BACKUP = "./task-manager-client/backup.base64";

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        if (bootstrap.getAuthEndpoint().getSocket() != null) {
            bootstrap.getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest());
        }
    }

    public void load() {
        if (bootstrap.getAuthEndpoint().getSocket() != null) {
            if (!Files.exists(Paths.get(FILE_BACKUP))) return;
            bootstrap.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
        }
    }

}
