package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    ICommandService getCommandService();
}
