package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.volkova.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Update task by index.";

    @NotNull
    private static final String NAME = "task-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
