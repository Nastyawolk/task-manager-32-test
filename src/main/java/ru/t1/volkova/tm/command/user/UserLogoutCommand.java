package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.volkova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Logout current user";

    @NotNull
    private static final String NAME = "logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        getAuthEndpoint().logout(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
