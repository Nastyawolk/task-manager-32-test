package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    private static final String NAME = "project-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(id, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
