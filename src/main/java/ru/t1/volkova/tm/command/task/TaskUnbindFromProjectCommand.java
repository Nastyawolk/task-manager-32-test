package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskID = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(projectID, taskID);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
